# -*- coding: utf-8 -*-
"""
Created on Tue Nov  3 15:03:25 2020

@author: Dimitri Nseng

"""

class BaseConnaissances:
    "Classe de la base de connaissances"
    
    def __init__(self, base):
        self.base = base
    
    def get_formules(self):
        if self.base is None:
            return self.recolte_formules()
        return self.base
    
    def recolte_formules(self) :
        # V : set
        V = set()
        V.add(self)
        self.formules = V
        return V

"""
phi ="et(a,b)"
phi1 = "ou(a,b)"
phi2 = "=>(<+>(a,b), Ou(Non(a), Non(b)))"
phi3 = "et(a, non(ou(b, c)))"
phi4 = "=>(Non(Non(a)), a)"
phi5 = "<=>(et(a,b), et(b,a))"
base = [phi, phi1, phi2, phi3, phi4, phi5]

B1 = BaseConnaissances(base)

#print(B1.get_formules())

for formule in BaseConnaissances(base).get_formules():
    print(formule)

"""